import com.google.common.io.Files
import java.lang.ProcessBuilder
import java.net.InetAddress
import java.net.UnknownHostException
import org.apache.commons.io.FileUtils
import org.nrg.xdat.XDAT
import org.nrg.xdat.entities.AliasToken
import org.nrg.xdat.services.AliasTokenService
import org.nrg.xdat.om.XnatExperimentdata

// Write available parameters out to a file
def f = new File('/tmp/' + dataId + '_' + scriptId + '-pipeline.out')
f.write("\nuser=" + user.getID())
f.append("\nscriptId=" + scriptId.toString())
f.append("\nevent=" + event.toString())
f.append("\nsrcEventId=" + srcEventId.toString())
f.append("\nsrcEventClass=" + srcEventClass.toString())
f.append("\nsrcWorkflowId=" + srcWorkflowId.toString())
f.append("\nscriptWorkflowId=" + scriptWorkflowId.toString())
f.append("\ndataType=" + dataType.toString())
f.append("\ndataId=" + dataId.toString())
f.append("\nexternalId=" + externalId.toString())
f.append("\nworkflow=" + workflow.toString())
f.append("\narguments = ")
f.append(arguments)

// Find the localhost where this will be running
def localhost = null
try {
    localhost = InetAddress.getLocalHost()
} catch (UnknownHostException unknownHostException) {
    println "ERROR: Unknown Host " + unknownHostException.getMessage()
    throw unknownHostException
}
f.append(localhost.toString())

// Check for empy argument json (auto-run) and set defaults
def args = null
if (arguments == "{}") {
    args = [:]
    args.scans = "all"
    args.existing = "skip"
    args.ref_session = "none"
    args.ref_scan = "none"
    args.execute = "sge"
} else {
    def slurper = new groovy.json.JsonSlurper()
    args = slurper.parseText(arguments)
}

f.append('\n' + "args = ")
f.append(args)

// Set up our parameters for the external script
def experimentData = XnatExperimentdata.getXnatExperimentdatasById(dataId, user, false)
def sessionLabel = experimentData.getLabel()
def projectId = externalId.toString()
def token = XDAT.getContextService().getBean(AliasTokenService.class).issueTokenForUser(user)
def tempDir = Files.createTempDir()

// Helper methods to execute our script
private def executeOnShell(String command, File workingDir) {
  println command
  def process = new ProcessBuilder(
      addShellPrefix(command)).directory(workingDir).redirectErrorStream(true).start()
  process.inputStream.eachLine { println it }
  process.waitFor();
  return process.exitValue()
}

private def addShellPrefix(String command) {
  commandArray = new String[3]
  commandArray[0] = "sh"
  commandArray[1] = "-c"
  commandArray[2] = command
  return commandArray
}

// Build external command and execute our script on the shell
def command = "source /nrgpackages/scripts/epd-python_setup.sh && " +
    "python /nrgpackages/tools.release/intradb/facemask/mask-session.py" +
    " -H https://" + localhost.getCanonicalHostName() +
    " -u " + token.getAlias() +
    " -p " + token.getSecret() +
    " -P " + projectId +
    " -e " + sessionLabel +
    " -c " + args.scans +
    " -x " + args.existing +
    " --ref-session " + args.ref_session +
    " --ref-scan " + args.ref_scan +
    " --submit-to " + args.execute
f.append('\n\n' + command)

try {
    def status = executeOnShell(command, tempDir)
    println "Return status: " + status
    if (status != 0) {
        println "\nFailed"
        return
    }
} catch (e) {
    println "EXCEPTION:  " + e
}

FileUtils.deleteDirectory(tempDir);
println "\nSuccess"
