#!/bin/bash
set -x

USER=${1}
PASS=${2}
HOST=${3}
PROJECT=${4}
SUBJECT=${5}
SESSION=${6}
SCAN=${7}
RESOURCE=${8}
REF_SCAN=${9}
BUILD_DIR=${10}
SCRIPT_DIR=${11}
USE_BET=${12}
INVASIVENESS=${13}
THRESHOLD=${14}
JSESSIONID=${15}

OLD_IFS="$IFS"
echo  "Determine ALT_HOST to try to work around http vs https firewall issues.  Always try to use http when possible."
if [[ "$HOST" =~ ^https:.* ]] ; then
	ALT_HOST="$HOST"
	HOST=$(echo "$ALT_HOST" | sed -e "s/https:/http:/" -e "s/\/\/[^\/]*/&:8080/")	
elif [[ "$HOST" =~ ^http:.* ]] ; then
	ALT_HOST=$(echo "$HOST" | sed -e "s/http:/https:/" -e "s/:8080//")	
else
	ALT_HOST="$HOST"
fi
echo "HOST=${HOST}, ALT_HOST=${ALT_HOST}"

source /opt/app/intradb/bash-utilities/common_functions.sh

main () {
  set_pcp_status
  setup_build_dir
  get_dicom
  # mask_face core script
  mask_dicom
  check_output
  post_dicom_orig
  post_defaced_dicom
  post_deface_qc
}

set_pcp_status () {
  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"
  uri=$HOST/xapi/pipelineControlPanel/project/$PROJECT/pipeline/FACEMASKING/entity/$SESSION/group/ALL/setValues?status=RUNNING
  curl -s -k --cookie "JSESSIONID=$JSESSIONID" $uri -X POST -H "Content-Type: application/json"
}

setup_build_dir () {
  # Set up build space
  zip_dir=$BUILD_DIR/zips
  input_dir=$BUILD_DIR/dicom_orig/$SCAN
  input_dir_relative=dicom_orig/$SCAN
  output_dir=$BUILD_DIR/dicom_deface/$SCAN
  mkdir -p $zip_dir
  mkdir -p $input_dir
  mkdir -p $output_dir
  # cd into build space since files are created in executing directory
  cd $BUILD_DIR
}

get_dicom () {
  # Get the DICOM from scan in zip archive and extract
  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"

  dicom_orig_zip=$zip_dir/${SESSION}_scan${SCAN}.zip
  scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN
  uri=$scan_uri/resources/$RESOURCE/files?format=zip
  doCurl -f -s -k --cookie "JSESSIONID=$JSESSIONID" $uri -o $dicom_orig_zip
  # Extract without directory structure
  unzip -o -j $dicom_orig_zip -d $input_dir
  rm $dicom_orig_zip
  ## Move files to top level directory
  find $input_dir -type f | xargs -I '{}' mv {} $input_dir
  ## Remove empty directories
  find $input_dir -type d | sort -u | xargs -I '{}' rmdir {}
  ## Recreate zip w/o dirs to re-upload as DICOM_ORIG
  #zip -r -j $dicom_orig_zip $input_dir
}

mask_dicom () {
  cd $BUILD_DIR
  # Either pass the ref scan or leave blank to avoid coregistraion if unnecessary
  # mask_face will assume we want to use itself for registration if not specified
  if [ "$REF_SCAN" != "none" ]; then
    reference_dir='$REF_SCAN'
    reference_param=' -r $REF_SCAN '
  fi

  source $SCRIPT_DIR/maskface_setup.sh
  cmd="$SCRIPT_DIR/bin/mask_face $input_dir_relative,$reference_dir -o $output_dir \
    $reference_param -b $USE_BET -e 1 -s $INVASIVENESS -t $THRESHOLD -um 0 \
    -roi 0 -ver 0"
  eval $cmd

  # Explicity check the exit status of mask_face and try a second time if failed
  exit_val="$?"
  if [ "$exit_val" -ne "0" ]; then
    echo "ERROR -- mask_face failed!"
    echo "mask_face exited with value $exit_val"
    echo "Retrying mask_face for scan."
    eval $cmd
  fi

  exit_val="$?"
  if [ "$exit_val" -ne "0" ]; then
    echo "ERROR -- mask_face failed a second time!"
    echo "mask_face exited with value $exit_val"
    exit 101
  fi
}

check_output () {
#  # And also check that the output exists before moving on
#  dicom_deface_zip=$output_dir/${SCAN}.zip
#
#  if [ ! -e $dicom_deface_zip ]; then
#    echo "${dicom_deface_zip} does not exist after mask_face"
#    echo "Exiting ..."
#    exit 102
#  fi
  OUTPUT_FILE_COUNT=`find $output_dir -type f | wc -l`
  INPUT_FILE_COUNT=`find $input_dir -type f | wc -l`
  if [ $OUTPUT_FILE_COUNT -lt $INPUT_FILE_COUNT ]; then
    echo "Output directory contains fewer files than input directory (OUTPUT_FILE_COUNT=$OUTPUT_FILE_COUNT, INPUT_FILE_COUNT=$INPUT_FILE_COUNT)"
    echo "Exiting ..."
    exit 102
  fi
}

post_dicom_orig () {
  # POST original non-defaced DICOM back as DICOM_ORIG unless this is a rerun
  # but skip if we are already operating on DICOM_ORIG resource

  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"

  if [ $RESOURCE = "DICOM" ]; then
    scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN
    uri=$scan_uri/resources/DICOM_ORIG
    curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT "$uri?format=DICOM&content=RAW"
    #if [ "$?" -ne "0" ]; then
    #  echo "ERROR -- Creating DICOM_ORIG resource failed!"
    #  echo "Curl PUT exited with value $exit_val"
    #  exit 103
    #fi
    sleep 30
    curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT $uri/files?replace\&reference=$input_dir
    #  IMPORTANT:  There seems to be a timeout for these reference scans that cannot be overridden with curl options.  
    #  It will return 52 if it takes too long to copy the data.  Let's accept this so processing doesn't stop
    RC="$?"
    if [ "$RC" -ne "0" ] && [ "$RC" -ne "52" ] ; then
      echo "ERROR -- Saving DICOM as DICOM_ORIG failed!"
      echo "Curl PUT exited with value $exit_val"
      exit 103
    fi
    sleep 90
  fi

}

post_defaced_dicom () {
  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"
 
  scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN
  dicom_uri=$scan_uri/resources/DICOM
  # Delete DICOM files
  if [ -d /data/intradb/archive/$PROJECT/arc001/$SESSION/SCANS/$SCAN/DICOM ] && [ `find /data/intradb/archive/$PROJECT/arc001/$SESSION/SCANS/$SCAN/DICOM_ORIG -type f | wc -l` -gt 1 ] ; then
      find /data/intradb/archive/$PROJECT/arc001/$SESSION/SCANS/$SCAN/DICOM -type f | grep -v "catalog.xml" | xargs -I '{}' rm {}
  fi
  curl -f -s -k --cookie JSESSIONID=$JSESSIONID -X POST "${HOST}/data/services/refresh/catalog?options=populateStats%2Cappend%2Cdelete%2Cchecksum&resource=/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/DICOM"
  sleep 30
  # PUT the defaced dicoms as DICOM resource
  curl -s -k --cookie "JSESSIONID=$JSESSIONID" -X POST $dicom_uri/files/?overwrite=true\&replace\&reference=$output_dir/$SCAN
  #  IMPORTANT:  There seems to be a timeout for these reference scans that cannot be overridden with curl options.  
  #  It will return 52 if it takes too long to copy the data.  Let's accept this so processing doesn't stop
  RC="$?"
  if [ "$RC" -ne "0" ] && [ "$RC" -ne "52" ] ; then
    echo "ERROR -- Saving defaced DICOM failed!"
    exit 104
  fi
  sleep 60
}

post_deface_qc () {
  # Collect the DEFACE_QC resource and POST them
  qc_zip=$BUILD_DIR/zips/${SCAN}_deface_qc.zip
  cd $BUILD_DIR/maskface/
  zip $qc_zip ${SCAN}_*.png
  zip $qc_zip ${SCAN}_*.out

  HOST_SESS=$(refresh_jsession)
  IFS="," read -a HSARR <<< $HOST_SESS
  JSESSIONID=${HSARR[0]}
  HOST=${HSARR[1]}
  IFS="$OLD_IFS"

  scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN
  qc_uri=$scan_uri/resources/DEFACE_QC
  if [ -d /data/intradb/archive/$PROJECT/arc001/$SESSION/SCANS/$SCAN/DEFACE_QC ] ; then
    # Try a DELETE in case already exists, harmless if it doesn't
    curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X DELETE $qc_uri?removeFiles=true
    sleep 90
  fi
  # Create the resource and POST files
  curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT $qc_uri?format=PNG
  sleep 60
  if [ ! -d /data/intradb/archive/$PROJECT/arc001/$SESSION/SCANS/$SCAN/DEFACE_QC ] ; then
    # Create the resource and POST files
    curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT $qc_uri?format=PNG
    sleep 60
  fi
  curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X POST $qc_uri/files?extract=true\&reference=$qc_zip
  sleep 30
  ## For some reason with the extract option, the catalog doesn't get updated.  We'll do that here.  NOTE:  It also returns a system status of "22"
  curl -s -k --cookie JSESSIONID=$JSESSIONID -X POST "${HOST}/data/services/refresh/catalog?options=populateStats%2Cappend%2Cdelete%2Cchecksum&resource=/archive/projects/${PROJECT}/subjects/${SUBJECT}/experiments/${SESSION}/scans/${SCAN}/resources/DEFACE_QC"
  #if [ "$?" -ne "0" ]; then
  #  echo "ERROR -- Saving DEFACE_QC failed!"
  #  exit 105
  #fi
  sleep 10
}


echo "Calling refresh_jsession"
HOST_SESS=$(refresh_jsession)
IFS="," read -a HSARR <<< $HOST_SESS
JSESSIONID=${HSARR[0]}
HOST=${HSARR[1]}
IFS="$OLD_IFS"

main
