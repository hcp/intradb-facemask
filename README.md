## Intradb Face Masking Pipeline

### Prerequisites
Install the following two modules in the same directory as this pipeline:
```sh
git clone https://github.com/revmic/hcpxnat
git clone https://bitbucket.org/mhileman/intradbpipelinecommon
mv intradbpipelinecommon intradbpipeline
```

---
### Launcher -- mask-session.py
Parent script that handles authentication and running/submitting the FaceMasking job for each scan to Sun Grid Engine or as a local process.

The pipeline assumes there is a DICOM resource, uploads it as DICOM_ORIG, and uploads the defaced images back as resource labeled DICOM. If it's a re-run, it checks for DICOM_ORIG resource and operates on that to preserve the non-defaced images.

Uses the face masking algorithm found here --> http://nrg.wustl.edu/software/face-masking/

##### Features:
* Gets session Json and decides which scans to mask.
* Gets auth token for use by per scan script.
* Creates build space path and passes along for input and output.
* Manages parent process logging and combines scan logs.
* Submits job per scan using one of the following methods:
    - Multi-proc - uses multiple processes on the same machine
    - SGE - uses qsub to submit to grid engine
    - TBD Docker - uses XNAT container service to submit to Docker container

##### Input parameters:
* hostname
* project
* subject
*--Optional, will determine from project/session if possible*
* session
* scans
*--Optional comma separated list of scans. If unused or "all", convert all scans.*
* submit-to (One of local or sge. Default local)
* existing
*--For existing defaced resources, either skip, overwrite, or fail. Default skip.*
* build-root
* log-root
* ref-session
*--Optional reference session containing scan to co-register. Uses self by default.*
* ref-scan
*--Optional reference scan used to co-register against Atlas. Uses self by default.*

---
### Subprocess -- mask-scan.sh

For one scan, pulls down DICOM resource, unzips, converts to NIFTI,
and POSTs back as a scan resource.
##### Input parameters:
* USER
* PASS
* HOST
* PROJECT
* SUBJECT
* SESSION
* SCAN
* RESOURCE
* REF_SCAN
* BUILD_DIR
* SCRIPT_DIR

---
#### Automation and Events -- xnat-automation.groovy
Add the contents of this as an XNAT automation script. Then, set up an XNAT event handler that will execute the pipeline for a particluar workflow, e.g., "Trasferred" or "Xsync". You can chain multiple pipelines together in this way.
